﻿using WebEntrega.Utils.Security;
using System.Text.RegularExpressions;
using WebEntrega.Utils.Validations;

namespace WebEntrega.Domain
{
    public class User
    {
        protected User() { }

        public User(string name, string email, string password)
        {
            AssertionConcern.AssertArgumentMinimumRange(name.Length , 3, "O nome deve conter mais de 3 caracteres");
            AssertionConcern.AssertArgumentTrue(Regex.IsMatch(email, @"[-0-9a-zA-Z.+_]+@[-0-9a-zA-Z.+_]+\.[a-zA-Z]{2,4}"), "E-mail inválido");
            AssertionConcern.AssertArgumentMinimumRange(password.Length , 6, "A senha deve conter pelo menos 6 caracteres");

            Id = 0;
            Name = name;
            Email = email;
            Password = EncryptHelper.Encrypt(password);
            IsActive = true;
        }

        public int Id { get; protected set; }
        public string Name { get; protected set; }
        public string Email { get; protected set; }
        public string Password { get; protected set; }
        public bool IsActive { get; protected set; }
        

        public void ChangePassword(string email, string password, string newPassword, string confirmNewPassword)
        {
            AssertionConcern.AssertArgumentTrue(Password != EncryptHelper.Encrypt(newPassword), "A nova senha não pode ser igual a anterior");
            AssertionConcern.AssertArgumentMinimumRange(newPassword.Length , 6, "A senha deve conter pelo menos 6 caracteres");
            AssertionConcern.AssertArgumentTrue(newPassword == confirmNewPassword, "As senhas informadas não coincidem");

            Authenticate(email, password);

            var newpass = EncryptHelper.Encrypt(newPassword);
            Password = newpass;
        }

        public string ResetPassword(string email)
        {
            AssertionConcern.AssertArgumentTrue(Email.ToLower() == email.ToLower(), "Usuário inválido");

            var password = System.Guid.NewGuid().ToString().Substring(0, 8);
            Password = EncryptHelper.Encrypt(password);

            return password;
        }

        public void Authenticate(string email, string password)
        {
            AssertionConcern.AssertArgumentTrue(IsActive, "Este usuário está inativo");
            AssertionConcern.AssertArgumentTrue(Email.ToLower() == email.ToLower(), "Usuário ou senha inválidos");
            AssertionConcern.AssertArgumentTrue(Password == EncryptHelper.Encrypt(password), "Usuário ou senha inválidos");
        }

        public void UpdateInfo(string name, string email)
        {
            AssertionConcern.AssertArgumentMinimumRange(name.Length , 3, "O nome deve conter mais de 3 caracteres");
            AssertionConcern.AssertArgumentTrue(Regex.IsMatch(email, @"[-0-9a-zA-Z.+_]+@[-0-9a-zA-Z.+_]+\.[a-zA-Z]{2,4}"), "E-mail inválido");

            Name = name;
            Email = email;
        }
        

        public void Inactivate()
        {
            IsActive = false;
        }

        public void Activate()
        {
            IsActive = true;
        }
    }
}
