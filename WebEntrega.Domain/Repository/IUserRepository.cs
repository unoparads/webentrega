﻿using System;

namespace WebEntrega.Domain.Repository
{
    public interface IUserRepository : IDisposable
    {
        User Get(string email);
        void SaveOrUpdate(User user);
        void Delete(int id);
    }
}
