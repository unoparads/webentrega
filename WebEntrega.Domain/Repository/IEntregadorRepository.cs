﻿using System;
using System.Collections.Generic;

namespace WebEntrega.Domain.Repository
{
    public interface IEntregadorRepository : IDisposable
    {
        Entregador Get(int id);
        IList<Entregador> GetAll();
        IList<Entregador> GetByName(string nome);
        IList<Entregador> GetByPhone(string telefone);
        void SaveOrUpdate(Entregador entregador);
        void Delete(int id);
    }
}
