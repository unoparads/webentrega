﻿using System;
using System.Collections.Generic;

namespace WebEntrega.Domain.Repository
{
    public interface ITamanhoRepository : IDisposable
    {
        Tamanho Get(int id);
        IList<Tamanho> GetAll();
        void SaveOrUpdate(Tamanho tamanho);
        void Delete(int id);
    }
}
