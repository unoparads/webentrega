﻿using System;
using System.Collections.Generic;

namespace WebEntrega.Domain.Repository
{
    public interface IEntregaRepository : IDisposable
    {
        IList<Entrega> GetById(int id);
        IList<Entrega> GetByEntregadorId(int entregadorId);
        IList<Entrega> GetByDate(DateTime dataInicial,DateTime dataFinal);
        void SaveOrUpdate(Entrega entrega);
        IList<Entrega> GetAll();
    }
}
