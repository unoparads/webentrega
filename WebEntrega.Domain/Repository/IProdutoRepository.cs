﻿using System;
using System.Collections.Generic;

namespace WebEntrega.Domain.Repository
{
    public interface IProdutoRepository:IDisposable
    {
        Produto Get(int id);
        IList<Produto> GetByName(string nome);
        IList<Produto> GetByDescricao(string descricao);
        IList<Produto> GetByTamanho(int tamanhoId);
        void SaveOrUpdate(Produto produto);
        void Delete(int id);
        IList<Produto> GetAll();
    }
}
