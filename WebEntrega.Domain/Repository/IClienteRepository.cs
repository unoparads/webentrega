﻿using System;
using System.Collections.Generic;

namespace WebEntrega.Domain.Repository
{
    public interface IClienteRepository:IDisposable
    {
        Cliente Get(int id);
        IList<Cliente> GetByName(string nome);
        IList<Cliente> GetByPhone(string telefone);
        void SaveOrUpdate(Cliente cliente);
        void Delete(int id);
        IList<Cliente> GetAll();
    }
}
