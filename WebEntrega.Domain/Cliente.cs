﻿using System.Collections.Generic;
using WebEntrega.Utils.Validations;

namespace WebEntrega.Domain
{
    public class Cliente
    {
        private IList<Entrega> _entregas;

        protected Cliente()
        {
        }

        public Cliente(string nome, string telefone, string endereco, string bairro, string cep, string referencia)
        {
            AssertionConcern.AssertArgumentMinimumRange(nome.Length, 3, "O nome deve conter mais de 3 caracteres");
            AssertionConcern.AssertArgumentMinimumRange(telefone.Length, 7, "O Telefone deve conter pelo menos 7 caracteres");
            AssertionConcern.AssertArgumentMinimumRange(endereco.Length, 4, "O Telefone deve conter pelo menos 4 caracteres");
            AssertionConcern.AssertArgumentMinimumRange(bairro.Length, 4, "O Telefone deve conter pelo menos 4 caracteres");
            AssertionConcern.AssertArgumentMinimumRange(cep.Length, 8, "O CEP deve conter pelo menos 8 caracteres");
            AssertionConcern.AssertArgumentMinimumRange(referencia.Length, 6, "A Referência deve conter mais de 6 caracteres");

            Id = 0;
            Nome = nome;
            Telefone = telefone;
            Endereco = endereco;
            Bairro = bairro;
            Cep = cep;
            Referencia = referencia;

            _entregas = new List<Entrega>();
        }

        public int Id { get; protected set; }
        public string Nome { get; protected set; }
        public string Telefone { get; protected set; }
        public string Endereco { get; protected set; }
        public string Bairro { get; protected set; }
        public string Cep { get; protected set; }
        public string Referencia { get; protected set; }
        public virtual ICollection<Entrega> Entregas
        {
            get { return _entregas; }
            protected set { _entregas = new List<Entrega>(value); }
        }
    }
}
