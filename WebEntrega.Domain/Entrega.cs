﻿using System;
using System.Collections.Generic;
using System.Linq;
using WebEntrega.Utils.Validations;

namespace WebEntrega.Domain
{
    public class Entrega
    {
        private IList<ItemEntrega> _itensEntrega;

        protected Entrega() { }

        public Entrega(int clienteId, int entregadorId)
        {
            AssertionConcern.AssertArgumentNotZeroOrNull(clienteId, "O código do cliente deve ser válido");
            AssertionConcern.AssertArgumentNotZeroOrNull(entregadorId, "O código do entregador deve ser válido");
            Id = 0;
            Entregue = false;
            ClienteId = clienteId;
            EntregadorId = entregadorId;
            _itensEntrega = new List<ItemEntrega>();
            Data = DateTime.Now;
        }

        public int Id { get; protected set; }
        public bool Entregue { get; protected set; }
        public int ClienteId { get; protected set; }
        public int EntregadorId { get; protected set; }
        public decimal Total { get; protected set; }
        public DateTime Data { get; protected set; }

        public void MarkAsEntregue()
        {
            Entregue = true;
        }

        public void MarkAsUnEntregue()
        {
            Entregue = false;
        }

        public void AddItem(ItemEntrega itemEntrega)
        {
            ItensEntrega.Add(itemEntrega);
            Total = ItensEntrega.Sum(c => c.Total);
        }

        public void RemoveItem(ItemEntrega itemEntrega)
        {
            ItensEntrega.Remove(itemEntrega);
            Total = ItensEntrega.Sum(c => c.Total);
        }

        public virtual ICollection<ItemEntrega> ItensEntrega
        {
            get { return _itensEntrega; }
            protected set { _itensEntrega = new List<ItemEntrega>(value); }
        }

    }
}
