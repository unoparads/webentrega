﻿using WebEntrega.Utils.Validations;

namespace WebEntrega.Domain
{
    public class Produto
    {
        protected Produto()
        {
            
        }

        public Produto(string nome,string descricao,int tamanhoId, decimal preco)
        {
            AssertionConcern.AssertArgumentMinimumRange(nome.Length, 5, "O Nome deve conter mais de 5 caracteres");
            AssertionConcern.AssertArgumentMinimumRange(descricao.Length, 5, "A descrição deve conter mais de 5 caracteres");
            AssertionConcern.AssertArgumentNotZeroOrNull(tamanhoId, "O Tamanho do produto deve ser válido");
            AssertionConcern.AssertArgumentNotZeroOrNull(preco, "O Preço do Produto deve ser válido");
            Id = 0;
            Nome = nome;
            Descricao = descricao;
            TamanhoId = tamanhoId;
            Preco = preco;
        }

        public int Id { get; protected set; }
        public string Nome { get; protected set; }
        public string Descricao { get; protected set; }
        public int TamanhoId { get; protected set; }
        public decimal Preco { get; protected set; }

        public void SetNome(string nome)
        {
            AssertionConcern.AssertArgumentMinimumRange(nome.Length, 5, "O Nome deve conter mais de 5 caracteres");
            Nome = nome;
        }

        public void SetDescricao(string descricao)
        {
            AssertionConcern.AssertArgumentMinimumRange(descricao.Length, 5, "A Descrição deve conter mais de 5 caracteres");
            Nome = descricao;
        }

        public void SetPreco(decimal preco)
        {
            AssertionConcern.AssertArgumentNotZeroOrNull(preco, "O Preco do Produto deve ser válido");
            Preco = preco;
        }

        public void SetTamanhoId(int tamanhoId)
        {
            AssertionConcern.AssertArgumentNotZeroOrNull(tamanhoId, "O Tamanho do produto deve ser válido");
            TamanhoId = tamanhoId;
        }
    }

}
