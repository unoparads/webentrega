﻿using WebEntrega.Utils.Validations;

namespace WebEntrega.Domain
{
    public class ItemEntrega
    {
        protected ItemEntrega()
        {
            
        }

        public ItemEntrega(int produtoId,decimal quantidade, decimal valor)
        {
            AssertionConcern.AssertArgumentNotZeroOrNull(produtoId, "O código do produto deve ser válido");
            AssertionConcern.AssertArgumentNotZeroOrNull(quantidade, "A Quantidade deve ser válida");
            AssertionConcern.AssertArgumentNotZeroOrNull(valor, "O Valor do Produto deve ser válido");
            Id = 0;
            ProdutoId = produtoId;
            Quantidade = quantidade;
            Valor = valor;
        }

        public int Id { get; protected set; }
        public int EntregaId { get; protected set; }
        public int ProdutoId { get; protected set; }
        public decimal Quantidade { get; protected set; }
        public decimal Valor { get; protected set; }
        public decimal Total
        {
            get { return Quantidade*Valor; }
        }

        public void SetQuantidade(decimal quantidade)
        {
            AssertionConcern.AssertArgumentNotZeroOrNull(quantidade,"A Quantidade deve ser válida");
            Quantidade = quantidade;
        }

        public void SetValor(decimal valor)
        {
            AssertionConcern.AssertArgumentNotZeroOrNull(valor, "O Valor do Produto deve ser válido");
            Valor = valor;
        }

        public void SetEntregaId(int entregaId)
        {
            AssertionConcern.AssertArgumentNotZeroOrNull(entregaId, "O Código da Entrega deve ser válido");
            EntregaId = entregaId;
        }
    }
}
