﻿using WebEntrega.Utils.Validations;

namespace WebEntrega.Domain
{
    public class Tamanho
    {
        protected Tamanho()
        {
            
        }

        public Tamanho(string descricao)
        {
            AssertionConcern.AssertArgumentMinimumRange(descricao.Length, 5, "A descrição deve conter mais de 5 caracteres");
            Id = 0;
            Descricao = descricao;
        }

        public int Id { get; protected set; }
        public string Descricao { get; protected set; }
    }
}
