﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace WebEntrega.Domain.Tests
{
    [TestClass]
    public class ClienteTests
    {
        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        [TestCategory("Entrega - Dada um novo cliente")]
        public void O_nome_deve_ser_valido()
        {
            new Cliente("", "", "", "", "", "");
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        [TestCategory("Entrega - Dada um novo cliente")]
        public void O_telefone_deve_ser_valido()
        {
            new Cliente("ast1", "", "", "", "", "");
        }


        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        [TestCategory("Entrega - Dada um novo cliente")]
        public void O_endereco_deve_ser_valido()
        {
            new Cliente("ast1", "12345678", "", "", "", "");
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        [TestCategory("Entrega - Dada um novo cliente")]
        public void O_bairro_deve_ser_valido()
        {
            new Cliente("ast1", "12345678", "1234", "", "", "");
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        [TestCategory("Entrega - Dada um novo cliente")]
        public void O_cep_deve_ser_valido()
        {
            new Cliente("ast1", "12345678", "1234", "1234", "", "");
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        [TestCategory("Entrega - Dada um novo cliente")]
        public void A_referencia_deve_ser_valida()
        {
            new Cliente("ast1", "12345678", "1234", "1234", "75645644", "");
        }
    }
}