﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using WebEntrega.Data.Repository;
using WebEntrega.Domain.Repository;
using Microsoft.Practices.Unity;
using WebEntrega.Utils.Dependency;

namespace WebEntrega.Domain.Tests
{
    [TestClass]
    public class DadaUmaNovaEntrega
    {

        [TestMethod]
        public void dado_uma_nova_entrega()
        {
            var entrega = new Entrega(1, 1);
            var itemEntrega = new ItemEntrega(1, 10, 10.8M);
            entrega.AddItem(itemEntrega);
            itemEntrega = new ItemEntrega(2, 5, 12.8M);
            entrega.AddItem(itemEntrega);
             DependencyFactory.Container.RegisterType<IEntregaRepository, EntregaRepository>(new HierarchicalLifetimeManager());

            var entregaReopsitory = DependencyFactory.Container.Resolve<IEntregaRepository>();
            entregaReopsitory.SaveOrUpdate(entrega);
        }
    }
}
