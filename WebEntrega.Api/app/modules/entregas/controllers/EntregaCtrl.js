﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('EntregaCtrl', EntregaCtrl);

    EntregaCtrl.$inject = ['$scope', 'EntregaRepository', '$location'];

    function EntregaCtrl($scope, entregaRepository, $location) {
        $scope.entrega = {
            id: 0,
            nome: '',
            endereco: '',
            telefone: '',
            bairro: '',
            cep: '',
            referencia: ''
        };

        function buscarTodasEntregas() {
            entregaRepository
                .getAllEntregas()
                .then(
                    function (result) {
                        $scope.entregas = result.data;
                    },
                    function (error) {
                        toastr.error(error.data, "Falha na requisição");
                    });
            
        }

        buscarTodasEntregas();

        $scope.init = function () {
            $scope.entrega = JSON.parse(window.sessionStorage.getItem('entrega'));
            window.sessionStorage.removeItem('entrega');
        }
        $scope.init();

        $scope.gravarEntrega = function () {
            gravarEntrega();
            New();
        }

        $scope.editarEntrega = function (entrega) {
            $scope.entrega = entrega;
            window.sessionStorage.setItem('entrega', JSON.stringify($scope.entrega));
            window.location.href = '#/entregas/editar';
        }

        $scope.novoEntrega = function () {
            New();
            window.sessionStorage.setItem('entrega', JSON.stringify($scope.entrega));
            window.location.href = '#/entregas/cadastrar';
        }

        $scope.contEntregas = function () {
            var count = 0;
            angular.forEach($scope.entregas, function () {
                count += 1;
            });
            return count;
        };

        $scope.remaining = function () {
            var count = 0;
            angular.forEach($scope.entregas, function (entrega) {
                count += entrega;
            });
            return count;
        };

        $scope.new = function () {
            New();
        }

        $scope.edit = function (entrega) {
            $scope.entrega = entrega;
        }

        $scope.delete = function (entrega) {
            var index = $scope.entregas.indexOf(entrega);
            $scope.entregas.splice(index, 1);
        }

        function gravarEntrega() {

            entregaRepository.cadastrarEntrega($scope.entrega).then(
                function (result) {
                    toastr.success(result, 'Cadastro efetuado com sucesso');
                    $location.path('#/entregas');
                },
                function (error) {
                    toastr.error(error.data, 'Falha no registro');
                });
        }

        $scope.removerEntrega = function (entrega) {
            entregaRepository.removerEntrega(entrega.id).then(
                function (result) {
                    toastr.success(result, 'Remoção efetuada com sucesso');
                    $location.path('#/entregas');
                },
                function (error) {
                    toastr.error(error.data, 'Falha na remoção');
                });
        }

        function New() {
            $scope.entrega = {
                id: 0,
                nome: '',
                endereco: '',
                telefone: '',
                bairro: '',
                cep: '',
                referencia: ''
            }
        }
    }
})();
