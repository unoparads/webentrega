﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('HomeCtrl', homeCtrl);

    homeCtrl.$inject = ['$scope', '$http', 'EntregaRepository'];

    function homeCtrl($scope, $http, entregaRepository) {
        $scope.entrega = {
            id: 0,
            text: '',
            Entregue: false
        }

        $scope.entregas = [];

        load();

        $scope.remaining = function () {
            var count = 0;
            angular.forEach($scope.entregas, function (entrega) {
                count += entrega.Entregue ? 0 : 1;
            });
            return count;
        };

        $scope.save = function (entrega) {
            if (entrega.id == 0) {
                save($scope.entrega);
            } else {
                Edit();
            }
            New();
        }

        $scope.new = function () {
            New();
        }

        $scope.edit = function (entrega) {
            $scope.entrega = entrega;
        }

        $scope.delete = function (entrega) {
            var index = $scope.entregas.indexOf(entrega)
            $scope.entregas.splice(index, 1);
        }

        $scope.sync = function () {
            sync();
        }

        function save(item) {
            item.id = $scope.entregas.length + 1;
            $scope.entregas.push(item);
        }

        function New() {
            $scope.entrega = {
                id: 0,
                text: '',
                Entregue: false
            }
        }

        function load() {
            if (navigator.onLine) {
                readCloud();
            } else {
                readLocal();
            }
        }

        function sync() {
            if (navigator.onLine) {
                saveCloud();
            } else {
                saveLocal();
            }
        }

        function readLocal() {
            $scope.entregas = angular.fromJson(localStorage.getItem("Entregas"));
            toastr.info('Informações carregadas do disco local...', 'Tudo certo!');
        }

        function saveLocal() {
            localStorage.setItem("Entregas", angular.toJson($scope.entregas));
            toastr.warning('Informações persistidas localmente...', 'Sincronizando');
        }

        function readCloud() {
            entregaRepository
                .getEntregas()
                .then(
                    function (result) {
                        $scope.entregas = result.data;
                    },
                    function (error) {
                        toastr.error(error.data, "Falha na requisição");
                    });
        }

        function saveCloud() {
            entregaRepository
                .sync($scope.entregas)
                .then(
                    function (result) {
                        toastr.info(result.data, "Sincronização completa")
                    },
                    function (error) {
                        toastr.error(error.data, "Falha na requisição");
                    });
        }
    }
})();
