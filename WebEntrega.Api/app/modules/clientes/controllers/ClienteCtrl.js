﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('ClienteCtrl', ClienteCtrl);

    ClienteCtrl.$inject = ['$scope', 'ClienteRepository', '$location'];

    function ClienteCtrl($scope, clienteRepository, $location) {
        $scope.cliente = {
            id: 0,
            nome: '',
            endereco: '',
            telefone: '',
            bairro: '',
            cep: '',
            referencia: ''
        };

        function buscarTodos() {
            clienteRepository
                .getAllClientes()
                .then(
                    function (result) {
                        $scope.clientes = result.data;
                    },
                    function (error) {
                        toastr.error(error.data, "Falha na requisição");
                    });
            
        }

        buscarTodos();

        $scope.init = function () {
            $scope.cliente = JSON.parse(window.sessionStorage.getItem('cliente'));
            window.sessionStorage.removeItem('cliente');
        }
        $scope.init();

        $scope.gravarCliente = function () {
            gravarCliente();
            New();
        }

        $scope.editarCliente = function (cliente) {
            $scope.cliente = cliente;
            window.sessionStorage.setItem('cliente', JSON.stringify($scope.cliente));
            window.location.href = '#/clientes/editar';
        }

        $scope.novoCliente = function () {
            New();
            window.sessionStorage.setItem('cliente', JSON.stringify($scope.cliente));
            window.location.href = '#/clientes/cadastrar';
        }

        $scope.contClientes = function () {
            var count = 0;
            angular.forEach($scope.clientes, function () {
                count += 1;
            });
            return count;
        };

        $scope.remaining = function () {
            var count = 0;
            angular.forEach($scope.clientes, function (cliente) {
                count += cliente;
            });
            return count;
        };

        $scope.new = function () {
            New();
        }

        $scope.edit = function (cliente) {
            $scope.cliente = cliente;
        }

        $scope.delete = function (cliente) {
            var index = $scope.clientes.indexOf(cliente);
            $scope.clientes.splice(index, 1);
        }

        function gravarCliente() {

            clienteRepository.cadastrarCliente($scope.cliente).then(
                function (result) {
                    toastr.success(result, 'Cadastro efetuado com sucesso');
                    $location.path('#/clientes');
                },
                function (error) {
                    toastr.error(error.data, 'Falha no registro');
                });
        }

        $scope.removerCliente = function (cliente) {
            clienteRepository.removerCliente(cliente.id).then(
                function (result) {
                    toastr.success(result, 'Remoção efetuada com sucesso');
                    $location.path('#/clientes');
                },
                function (error) {
                    toastr.error(error.data, 'Falha na remoção');
                });
        }

        function New() {
            $scope.cliente = {
                id: 0,
                nome: '',
                endereco: '',
                telefone: '',
                bairro: '',
                cep: '',
                referencia: ''
            }
        }
    }
})();
