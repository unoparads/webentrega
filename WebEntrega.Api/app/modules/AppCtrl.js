﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('AppCtrl', appCtrl);

    appCtrl.$inject = ['$rootScope', '$scope', '$location', 'UserRepository'];

    function appCtrl($rootScope, $scope, $location, userRepository) {
        $rootScope.changeTheme = function (theme) {
            $rootScope.theme = theme;
            localStorage.setItem('theme', theme);
        };

        $scope.logout = function () {
            userRepository.clearUserData();
        }
    }
})();