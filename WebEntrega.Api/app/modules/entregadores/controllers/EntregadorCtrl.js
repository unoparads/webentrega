﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('EntregadorCtrl', EntregadorCtrl);

    EntregadorCtrl.$inject = ['$scope', 'EntregadorRepository', '$location'];

    function EntregadorCtrl($scope, entregadorRepository, $location) {
        $scope.entregador = {
            id: 0,
            nome: '',
            cnpj: '',
            endereco: '',
            telefone: '',
            bairro: '',
            cep: '',
            email: ''
        };

        function buscarTodos() {
            entregadorRepository
                .getAllEntregadors()
                .then(
                    function (result) {
                        $scope.entregadors = result.data;
                    },
                    function (error) {
                        toastr.error(error.data, "Falha na requisição");
                    });
            
        }

        buscarTodos();

        $scope.init = function () {
            $scope.entregador = JSON.parse(window.sessionStorage.getItem('entregador'));
            window.sessionStorage.removeItem('entregador');
        }
        $scope.init();

        $scope.gravarEntregador = function () {
            gravarEntregador();
            New();
        }

        $scope.editarEntregador = function (entregador) {
            $scope.entregador = entregador;
            window.sessionStorage.setItem('entregador', JSON.stringify($scope.entregador));
            window.location.href = '#/entregadores/editar';
        }

        $scope.novoEntregador = function () {
            New();
            window.sessionStorage.setItem('entregador', JSON.stringify($scope.entregador));
            window.location.href = '#/entregadors/cadastrar';
        }

        $scope.contEntregadores = function () {
            var count = 0;
            angular.forEach($scope.entregadores, function () {
                count += 1;
            });
            return count;
        };

        $scope.remaining = function () {
            var count = 0;
            angular.forEach($scope.entregadores, function (entregador) {
                count += entregador;
            });
            return count;
        };

        $scope.new = function () {
            New();
        }

        $scope.edit = function (entregador) {
            $scope.entregador = entregador;
        }

        $scope.delete = function (entregador) {
            var index = $scope.entregadores.indexOf(entregador);
            $scope.entregadores.splice(index, 1);
        }

        function gravarEntregador() {

            entregadorRepository.cadastrarEntregador($scope.entregador).then(
                function (result) {
                    toastr.success(result, 'Cadastro efetuado com sucesso');
                    $location.path('#/entregadores');
                },
                function (error) {
                    toastr.error(error.data, 'Falha no registro');
                });
        }

        $scope.removerEntregadores = function (entregador) {
            entregadorRepository.removerEntregador(entregador.id).then(
                function (result) {
                    toastr.success(result, 'Remoção efetuada com sucesso');
                    $location.path('#/entregadors');
                },
                function (error) {
                    toastr.error(error.data, 'Falha na remoção');
                });
        }

        function New() {
            $scope.entregador = {
                id: 0,
                nome: '',
                cnpj: '',
                endereco: '',
                telefone: '',
                bairro: '',
                cep: '',
                email: ''
            }
        }
    }
})();
