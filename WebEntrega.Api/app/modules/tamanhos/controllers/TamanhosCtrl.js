﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('TamanhoCtrl', TamanhoCtrl);

    TamanhoCtrl.$inject = ['$scope', 'TamanhoRepository', '$location'];

    function TamanhoCtrl($scope, tamanhoRepository, $location) {
        $scope.tamanho = {
            id: 0,
            nome: '',
            endereco: '',
            telefone: '',
            bairro: '',
            cep: '',
            referencia: ''
        };

        function buscarTodos() {
            tamanhoRepository
                .getAllTamanhos()
                .then(
                    function (result) {
                        $scope.tamanhos = result.data;
                    },
                    function (error) {
                        toastr.error(error.data, "Falha na requisição");
                    });
            
        }

        buscarTodos();

        $scope.init = function () {
            $scope.tamanho = JSON.parse(window.sessionStorage.getItem('tamanho'));
            window.sessionStorage.removeItem('tamanho');
        }
        $scope.init();

        $scope.gravarTamanho = function () {
            gravarTamanho();
            New();
        }

        $scope.editarTamanho = function (tamanho) {
            $scope.tamanho = tamanho;
            window.sessionStorage.setItem('tamanho', JSON.stringify($scope.tamanho));
            window.location.href = '#/tamanhos/editar';
        }

        $scope.novoTamanho = function () {
            New();
            window.sessionStorage.setItem('tamanho', JSON.stringify($scope.tamanho));
            window.location.href = '#/tamanhos/cadastrar';
        }

        $scope.contTamanhos = function () {
            var count = 0;
            angular.forEach($scope.tamanhos, function () {
                count += 1;
            });
            return count;
        };

        $scope.remaining = function () {
            var count = 0;
            angular.forEach($scope.tamanhos, function (tamanho) {
                count += tamanho;
            });
            return count;
        };

        $scope.new = function () {
            New();
        }

        $scope.edit = function (tamanho) {
            $scope.tamanho = tamanho;
        }

        $scope.delete = function (tamanho) {
            var index = $scope.tamanhos.indexOf(tamanho);
            $scope.tamanhos.splice(index, 1);
        }

        function gravarTamanho() {

            tamanhoRepository.cadastrarTamanho($scope.tamanho).then(
                function (result) {
                    toastr.success(result, 'Cadastro efetuado com sucesso');
                    $location.path('#/tamanhos');
                },
                function (error) {
                    toastr.error(error.data, 'Falha no registro');
                });
        }

        $scope.removerTamanho = function (tamanho) {
            tamanhoRepository.removerTamanho(tamanho.id).then(
                function (result) {
                    toastr.success(result, 'Remoção efetuada com sucesso');
                    $location.path('#/tamanhos');
                },
                function (error) {
                    toastr.error(error.data, 'Falha na remoção');
                });
        }

        function New() {
            $scope.tamanho = {
                id: 0,
                nome: '',
                endereco: '',
                telefone: '',
                bairro: '',
                cep: '',
                referencia: ''
            }
        }
    }
})();
