﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('ProdutoCtrl', ProdutoCtrl);

    ProdutoCtrl.$inject = ['$scope', 'ProdutoRepository','TamanhoRepository', '$location'];

    function ProdutoCtrl($scope, produtoRepository,tamanhoRepository, $location) {
        $scope.produto = {
            id: 0,
            nome: '',
            descricao: '',
            tamanhoId: '',
            preco: ''
        };
        function buscarTodosTamanhos() {
            tamanhoRepository
                .getAllTamanhos()
                .then(
                    function (result) {
                        $scope.tamanhos = result.data;
                    },
                    function (error) {
                        toastr.error(error.data, "Falha na requisição");
                    });

        }

        $scope.MudarTamanho = function (tamanho) {
            $scope.produto.tamanhoId = tamanho.id;
        };

        function buscarTodos() {
            produtoRepository
                .getAllProdutos()
                .then(
                    function (result) {
                        $scope.produtos = result.data;
                    },
                    function (error) {
                        toastr.error(error.data, "Falha na requisição");
                    });
            buscarTodosTamanhos();
        }

        buscarTodos();

        $scope.init = function () {
            $scope.produto = JSON.parse(window.sessionStorage.getItem('produto'));
            window.sessionStorage.removeItem('produto');
        }
        $scope.init();

        $scope.gravarProduto = function () {
            gravarProduto();
            New();
        }

        $scope.editarProduto = function (produto) {
            $scope.produto = produto;
            window.sessionStorage.setItem('produto', JSON.stringify($scope.produto));
            window.location.href = '#/produtos/editar';
        }

        $scope.novoProduto = function () {
            New();
            window.sessionStorage.setItem('produto', JSON.stringify($scope.produto));
            window.location.href = '#/produtos/cadastrar';
        }

        $scope.contProdutos = function () {
            var count = 0;
            angular.forEach($scope.produtos, function () {
                count += 1;
            });
            return count;
        };

        $scope.remaining = function () {
            var count = 0;
            angular.forEach($scope.produtos, function (produto) {
                count += produto;
            });
            return count;
        };

        $scope.new = function () {
            New();
        }

        $scope.edit = function (produto) {
            $scope.produto = produto;
        }

        $scope.delete = function (produto) {
            var index = $scope.produtos.indexOf(produto);
            $scope.produtos.splice(index, 1);
        }

        function gravarProduto() {

            produtoRepository.cadastrarProduto($scope.produto).then(
                function (result) {
                    toastr.success(result, 'Cadastro efetuado com sucesso');
                    $location.path('#/produtos');
                },
                function (error) {
                    toastr.error(error.data, 'Falha no registro');
                });
        }

        $scope.removerProduto = function (produto) {
            produtoRepository.removerProduto(produto.id).then(
                function (result) {
                    toastr.success(result, 'Remoção efetuada com sucesso');
                    $location.path('#/produtos');
                },
                function (error) {
                    toastr.error(error.data, 'Falha na remoção');
                });
        }

        function New() {
            $scope.produto = {
                id: 0,
                nome: '',
                endereco: '',
                telefone: '',
                bairro: '',
                cep: '',
                referencia: ''
            }
        }
    }
})();
