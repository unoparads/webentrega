﻿(function () {
    'use strict';
    var app = angular.module('app', ['ngResource', 'ngRoute', 'ngAnimate', 'ui.bootstrap']);

    app.config(function ($routeProvider, $locationProvider) {
        $routeProvider
            .when('/', {
                controller: 'HomeCtrl',
                templateUrl: 'modules/home/views/index.html',
                requireLogin: true
            })
            .when('/account/login', {
                controller: 'LoginCtrl',
                templateUrl: 'modules/account/views/login.html',
                requireLogin: false
            })
            .when('/account/logout', {
                controller: 'LogoutCtrl',
                templateUrl: 'modules/account/views/logout.html',
                requireLogin: false
            })
            .when('/account/signup', {
                controller: 'SignUpCtrl',
                templateUrl: 'modules/account/views/signup.html',
                requireLogin: false
            })
            .when('/account/forgot-password', {
                controller: 'ForgotPasswordCtrl',
                templateUrl: 'modules/account/views/forgot-password.html',
                requireLogin: false
            })
            .when('/clientes/cadastrar', {
                controller: 'ClienteCtrl',
                templateUrl: 'modules/clientes/views/cadastrar.html',
                requireLogin: true
            })
            .when('/clientes/editar', {
                controller: 'ClienteCtrl',
                templateUrl: 'modules/clientes/views/editar.html',
                requireLogin: true
            })
            .when('/clientes', {
                controller: 'ClienteCtrl',
                templateUrl: 'modules/clientes/views/index.html',
                requireLogin: true
            })
            .when('/produtos/cadastrar', {
                controller: 'ProdutoCtrl',
                templateUrl: 'modules/produtos/views/cadastrar.html',
                requireLogin: true
            })
            .when('/produtos/editar', {
                controller: 'ProdutoCtrl',
                templateUrl: 'modules/produtos/views/editar.html',
                requireLogin: true
            })
            .when('/produtos', {
                controller: 'ProdutoCtrl',
                templateUrl: 'modules/produtos/views/index.html',
                requireLogin: true
            })
            .when('/entregadores/cadastrar', {
                controller: 'EntregadorCtrl',
                templateUrl: 'modules/entregadores/views/cadastrar.html',
                requireLogin: true
            })
            .when('/entregadores/editar', {
                controller: 'EntregadorCtrl',
                templateUrl: 'modules/entregadores/views/editar.html',
                requireLogin: true
            })
            .when('/entregadores', {
                controller: 'EntregadorCtrl',
                templateUrl: 'modules/entregadores/views/index.html',
                requireLogin: true
            })
            .when('/entregas/cadastrar', {
                controller: 'EntregaCtrl',
                templateUrl: 'modules/entregas/views/cadastrar.html',
                requireLogin: true
            })
            .when('/entregas/editar', {
                controller: 'EntregaCtrl',
                templateUrl: 'modules/entregas/views/editar.html',
                requireLogin: true
            })
            .when('/entregas', {
                controller: 'EntregaCtrl',
                templateUrl: 'modules/entregas/views/index.html',
                requireLogin: true
            })
            .when('/tamanhos/cadastrar', {
                controller: 'TamanhoCtrl',
                templateUrl: 'modules/tamanhos/views/cadastrar.html',
                requireLogin: true
            })
            .when('/tamanhos/editar', {
                controller: 'TamanhoCtrl',
                templateUrl: 'modules/tamanhos/views/editar.html',
                requireLogin: true
            })
            .when('/tamanhos', {
                controller: 'TamanhoCtrl',
                templateUrl: 'modules/tamanhos/views/index.html',
                requireLogin: true
            })
            .otherwise({
                controller: 'HomeCtrl as vm',
                templateUrl: 'modules/home/views/404.html',
                requireLogin: true
            });
    });

    app.run(['$http', '$rootScope', '$location', 'UserRepository', function ($http, $rootScope, $location, userRepository) {
        // Usuário logado        
        $rootScope.isAuthorized = false;
        $rootScope.user = {
            name: '',
            email: ''
        };

        // Verifica se não existe um token
        if (!localStorage.getItem('token')) {
            localStorage.setItem('token', '');
        } else {
            $rootScope.isAuthorized = true;
            userRepository.setCurrentProfile();
        }

        // Verifica o tema
        if (!localStorage.getItem('theme')) {
            localStorage.setItem('theme', 'journal');
            $rootScope.theme = 'journal';
        } else {
            $rootScope.theme = localStorage.getItem('theme');
        }

        $rootScope.$on("$routeChangeStart", function (event, next, current) {
            if (next.requireLogin && $rootScope.isAuthorized == false) {
                $location.path('/account/login');
            }
        });
    }]);
})();