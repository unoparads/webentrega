﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('LogoutCtrl', logoutCtrl);

    logoutCtrl.$inject = ['$location', 'UserRepository'];

    function logoutCtrl($location, userRepository) {
        userRepository.clearUserData();
        $location.path('/login');
    }
})();
