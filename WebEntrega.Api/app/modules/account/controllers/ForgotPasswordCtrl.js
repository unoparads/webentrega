﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('ForgotPasswordCtrl', forgotPasswordCtrl);

    forgotPasswordCtrl.$inject = ['$scope', 'UserRepository', '$location'];

    function forgotPasswordCtrl($scope, userRepository, $location) {
        $scope.forgotPasswordModel = {
            email: ''
        };

        $scope.rememberPassword = function () {
            userRepository.resetPassword($scope.forgotPasswordModel).then(
                function (result) {
                    toastr.success(result.data, 'Senha restaurada com sucesso!');
                    $location.path('/forgot-paswword-sucess');
                },
                function (error) {
                    toastr.error(error.data, 'Falha no registro');
                });
        }
    }
})();
