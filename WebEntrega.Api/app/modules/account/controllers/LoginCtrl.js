﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('LoginCtrl', loginCtrl);

    loginCtrl.$inject = ['$rootScope', '$scope', '$location', 'AuthRepository', 'UserRepository'];

    function loginCtrl($rootScope, $scope, $location, authRepository, userRepository) {

        $scope.login = {
            email: '',
            password: ''
        }

        $scope.authenticate = function () {
            var promise = authRepository.getToken($scope.login.email, $scope.login.password).then(
                   function (result) {
                       localStorage.setItem('token', result.data.access_token);
                       $rootScope.isAuthorized = true;
                       userRepository.setCurrentProfile();
                       $location.path('/');
                   },
                   function (error) {
                       toastr.error(error.data.error_description, 'Falha na autenticação');
                   });
        }
    }
})();
