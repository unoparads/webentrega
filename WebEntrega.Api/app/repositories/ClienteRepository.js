﻿(function () {
    'use strict';

    angular
        .module('app')
        .factory('ClienteRepository', clienteRepository);

    clienteRepository.$inject = ['$http', '$rootScope', '$location'];

    function clienteRepository($http, $rootScope, $location) {
        return {
            getAllClientes: function () {
                return $http.get("/api/clientes", { headers: { 'Authorization': 'Bearer ' + localStorage.getItem('token') } });
            },
            getClientesById: function (id) {
                return $http.get("/api/clientes/" + id, { headers: { 'Authorization': 'Bearer ' + localStorage.getItem('token') } });
            },
            cadastrarCliente: function (cadastrarClienteModel) {
                return $http.post("/api/clientes", cadastrarClienteModel, { headers: { 'Authorization': 'Bearer ' + localStorage.getItem('token') } });
            },
            removerCliente: function (id) {
                return $http.delete("/api/clientes/" + id, { headers: { 'Authorization': 'Bearer ' + localStorage.getItem('token') } });
            },
        };
    }
})();