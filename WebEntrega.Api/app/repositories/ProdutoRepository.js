﻿(function () {
    'use strict';

    angular
        .module('app')
        .factory('ProdutoRepository', produtoRepository);

    produtoRepository.$inject = ['$http', '$rootScope', '$location'];

    function produtoRepository($http, $rootScope, $location) {
        return {
            getAllProdutos: function () {
                return $http.get("/api/Produtos", { headers: { 'Authorization': 'Bearer ' + localStorage.getItem('token') } });
            },
            getProdutoById: function (id) {
                return $http.get("/api/Produtos/" + id, { headers: { 'Authorization': 'Bearer ' + localStorage.getItem('token') } });
            },
            cadastrarProduto: function (cadastrarProdutoModel) {
                return $http.post("/api/Produtos", cadastrarProdutoModel, { headers: { 'Authorization': 'Bearer ' + localStorage.getItem('token') } });
            },
            removerProduto: function (id) {
                return $http.delete("/api/Produtos/" + id, { headers: { 'Authorization': 'Bearer ' + localStorage.getItem('token') } });
            },
        };
    }
})();