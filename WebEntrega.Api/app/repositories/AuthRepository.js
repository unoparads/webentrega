﻿(function () {
    'use strict';

    angular
        .module('app')
        .factory('AuthRepository', authRepository);

    authRepository.$inject = ['$http', '$rootScope', 'UserRepository'];

    function authRepository($http, $rootScope, userRepository) {
        return {
            getToken: function (username, password) {
                var data = "grant_type=password&username=" + username + "&password=" + password;
                return $http.post("/api/security/token", data, { headers: { 'Content-Type': 'application/x-www-form-urlencoded' } });
            }
        };
    }
})();