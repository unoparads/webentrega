﻿(function () {
    'use strict';

    angular
        .module('app')
        .factory('EntregaRepository', entregaRepository);

    entregaRepository.$inject = ['$http', '$rootScope', '$location'];

    function entregaRepository($http, $rootScope, $location) {
        return {
            getEntregas: function () {
                return $http.get("/api/entregas", { headers: { 'Authorization': 'Bearer ' + localStorage.getItem('token') } });
            },
            sync: function (entregas) {
                return $http.post("/api/entregas", entregas, { headers: { 'Authorization': 'Bearer ' + localStorage.getItem('token') } });
            },
        };
    }
})();