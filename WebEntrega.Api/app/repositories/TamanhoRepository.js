﻿(function () {
    'use strict';

    angular
        .module('app')
        .factory('TamanhooRepository', tamanhoRepository);

    tamanhoRepository.$inject = ['$http', '$rootScope', '$location'];

    function tamanhoRepository($http, $rootScope, $location) {
        return {
            getAllTamanhos: function () {
                return $http.get("/api/Tamanhos", { headers: { 'Authorization': 'Bearer ' + localStorage.getItem('token') } });
            },
            getTamanhoById: function (id) {
                return $http.get("/api/Tamanhos/" + id, { headers: { 'Authorization': 'Bearer ' + localStorage.getItem('token') } });
            },
            cadastrarTamanho: function (tamanho) {
                return $http.post("/api/Tamanhos", tamanho, { headers: { 'Authorization': 'Bearer ' + localStorage.getItem('token') } });
            },
            removerTamanho: function (id) {
                return $http.delete("/api/Tamanhos/" + id, { headers: { 'Authorization': 'Bearer ' + localStorage.getItem('token') } });
            },
        };
    }
})();