﻿(function () {
    'use strict';

    angular
        .module('app')
        .factory('EntregadorRepository', entregadorRepository);

    entregadorRepository.$inject = ['$http', '$rootScope', '$location'];

    function entregadorRepository($http, $rootScope, $location) {
        return {
            getAllEntregadores: function () {
                return $http.get("/api/entregadores", { headers: { 'Authorization': 'Bearer ' + localStorage.getItem('token') } });
            },
            getEntregadorById: function (id) {
                return $http.get("/api/entregadores/" + id, { headers: { 'Authorization': 'Bearer ' + localStorage.getItem('token') } });
            },
            cadastrarEntregador: function (entregador) {
                return $http.post("/api/entregadores", entregador, { headers: { 'Authorization': 'Bearer ' + localStorage.getItem('token') } });
            },
            removerEntregador: function (id) {
                return $http.delete("/api/entregadores/" + id, { headers: { 'Authorization': 'Bearer ' + localStorage.getItem('token') } });
            },
        };
    }
})();