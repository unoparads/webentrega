﻿using Microsoft.Practices.Unity;
using WebEntrega.Data.Context;
using WebEntrega.Data.Repository;
using WebEntrega.Domain.Repository;
using WebEntrega.Utils.Dependency;

namespace WebEntrega.Api
{
    public static class DependencyResolver
    {
        public static void Resolve()
        {
            DependencyFactory.Container.RegisterType<WebEntregaDataContext, WebEntregaDataContext>(new HierarchicalLifetimeManager());
            DependencyFactory.Container.RegisterType<IUserRepository, UserRepository>(new HierarchicalLifetimeManager());
            DependencyFactory.Container.RegisterType<IEntregaRepository, EntregaRepository>(new HierarchicalLifetimeManager());
            DependencyFactory.Container.RegisterType<IClienteRepository, ClienteRepository>(new HierarchicalLifetimeManager());
            DependencyFactory.Container.RegisterType<IProdutoRepository, ProdutoRepository>(new HierarchicalLifetimeManager());
            DependencyFactory.Container.RegisterType<IEntregadorRepository, EntregadorRepository>(new HierarchicalLifetimeManager());
            DependencyFactory.Container.RegisterType<ITamanhoRepository, TamanhoRepository>(new HierarchicalLifetimeManager());
        }
    }
}