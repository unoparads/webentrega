﻿namespace WebEntrega.Api.Models.Produtos
{
    public class AddProdutoModel
    {
        public string Nome { get; set; }
        public string Descricao { get; set; }
        public int TamanhoId { get; set; }
        public decimal Preco { get; set; }
    }
}