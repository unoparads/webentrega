﻿namespace WebEntrega.Api.Models.Account
{
    public class ResetPasswordViewModel
    {
        public string Email { get; set; }
    }
}