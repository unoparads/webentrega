﻿namespace WebEntrega.Api.Models.Entrega
{
    public class SyncEntregaViewModel
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public bool Entregue { get; set; }
    }
}