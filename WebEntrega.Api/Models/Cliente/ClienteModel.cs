﻿namespace WebEntrega.Api.Models.Cliente
{
    public class AddClienteModel
    {
        public string Nome { get; set; }
        public string Telefone { get; set; }
        public string Endereco { get; set; }
        public string Bairro { get; set; }
        public string Cep { get; set; }
        public string Referencia { get; set; }
    }
}