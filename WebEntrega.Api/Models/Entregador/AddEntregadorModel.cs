﻿namespace WebEntrega.Api.Models.Entregador
{
    public class AddEntregadorModel
    {
        public string Nome { get; set; }
        public string Telefone { get; set; }
        public string Cnpj { get; set; }
        public string Endereco { get; set; }
        public string Bairro { get; set; }
        public string Cep { get; set; }
        public string email { get; set; }
    }
}