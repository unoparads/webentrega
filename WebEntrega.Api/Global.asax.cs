﻿using WebEntrega.Data.Context;
using System;
using System.Web;

namespace WebEntrega.Api
{
    public class WebApiApplication : HttpApplication
    {
        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            HttpContext.Current.Items["_EntityContext"] = new WebEntregaDataContext();
        }

        protected void Application_EndRequest(object sender, EventArgs e)
        {
            var entityContext = HttpContext.Current.Items["_EntityContext"] as WebEntregaDataContext;
            if (entityContext != null)
                entityContext.Dispose();
        }
    }
}
