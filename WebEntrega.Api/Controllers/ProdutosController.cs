﻿using System;
using System.Data.Entity.Infrastructure;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using WebEntrega.Api.Models.Produtos;
using WebEntrega.Domain;
using WebEntrega.Domain.Repository;
using WebEntrega.Utils.Helpers;

namespace WebEntrega.Api.Controllers
{
    [RoutePrefix("api/produtos")]
    public class ProdutoController : ApiController
    {
        private readonly IProdutoRepository _produtoRepository;

        public ProdutoController(IProdutoRepository produtoRepository)
        {
            _produtoRepository = produtoRepository;
        }

        [HttpGet]
        [Route("")]
        public Task<HttpResponseMessage> GetAll()
        {
            HttpResponseMessage response;

            try
            {
                var produtos = _produtoRepository.GetAll();
                response = Request.CreateResponse(HttpStatusCode.OK, produtos);
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
                LogErrorHelper.Register(ex);
            }

            var tsc = new TaskCompletionSource<HttpResponseMessage>();
            tsc.SetResult(response);
            return tsc.Task;
        }

        [HttpGet]
        [Route("{id}")]
        public Task<HttpResponseMessage> Get(int id)
        {
            HttpResponseMessage response;

            try
            {
                var produtos = _produtoRepository.Get(id);
                response = Request.CreateResponse(HttpStatusCode.OK, produtos);
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
                LogErrorHelper.Register(ex);
            }

            var tsc = new TaskCompletionSource<HttpResponseMessage>();
            tsc.SetResult(response);
            return tsc.Task;
        }
        
        [HttpGet]
        [Route("nome/{nome}")]
        public Task<HttpResponseMessage> GetByName(string nome)
        {
            HttpResponseMessage response;

            try
            {
                var produtos = _produtoRepository.GetByName(nome);
                response = Request.CreateResponse(HttpStatusCode.OK, produtos);
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
                LogErrorHelper.Register(ex);
            }

            var tsc = new TaskCompletionSource<HttpResponseMessage>();
            tsc.SetResult(response);
            return tsc.Task;
        }

        [HttpPost]
        [Route("")]
        public Task<HttpResponseMessage> Cadastrar(AddProdutoModel model)
        {
            var response = new HttpResponseMessage();

            try
            {
                var produtos = new Produto(model.Nome, model.Descricao, model.TamanhoId, model.Preco);
                _produtoRepository.SaveOrUpdate(produtos);
                response = Request.CreateResponse(HttpStatusCode.OK, "Informações salvas com sucesso!");
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
                LogErrorHelper.Register(ex);
            }

            var tsc = new TaskCompletionSource<HttpResponseMessage>();
            tsc.SetResult(response);
            return tsc.Task;
        }

        [HttpPut]
        [Authorize]
        [Route("")]
        public Task<HttpResponseMessage> Update(Produto produto)
        {
            var response = new HttpResponseMessage();

            try
            {
                _produtoRepository.SaveOrUpdate(produto);

                response = Request.CreateResponse(HttpStatusCode.OK, "Informações salvas com sucesso!");
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
                LogErrorHelper.Register(ex);
            }

            var tsc = new TaskCompletionSource<HttpResponseMessage>();
            tsc.SetResult(response);
            return tsc.Task;
        }


        [HttpDelete]
        [Authorize]
        [Route("")]
        public Task<HttpResponseMessage> Delete(int produtoId)
        {
            var response = new HttpResponseMessage();

            try
            {
                _produtoRepository.Delete(produtoId);

                response = Request.CreateResponse(HttpStatusCode.OK, "Informações removidas com sucesso!");
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
                LogErrorHelper.Register(ex);
            }

            var tsc = new TaskCompletionSource<HttpResponseMessage>();
            tsc.SetResult(response);
            return tsc.Task;
        }


        protected override void Dispose(bool disposing)
        {
            _produtoRepository.Dispose();
        }
    }
}