﻿using System;
using System.Data.Entity.Infrastructure;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using WebEntrega.Api.Models.Cliente;
using WebEntrega.Domain;
using WebEntrega.Domain.Repository;
using WebEntrega.Utils.Helpers;

namespace WebEntrega.Api.Controllers
{
    [RoutePrefix("api/clientes")]
    public class ClienteController : ApiController
    {
        private readonly IClienteRepository _clienteRepository;

        public ClienteController(IClienteRepository clienteRepository)
        {
            _clienteRepository = clienteRepository;
        }

        [HttpGet]
        [Route("")]
        public Task<HttpResponseMessage> GetAll()
        {
            HttpResponseMessage response;

            try
            {
                var cliente = _clienteRepository.GetAll();
                response = Request.CreateResponse(HttpStatusCode.OK, cliente);
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
                LogErrorHelper.Register(ex);
            }

            var tsc = new TaskCompletionSource<HttpResponseMessage>();
            tsc.SetResult(response);
            return tsc.Task;
        }

        [HttpGet]
        [Route("{id}")]
        public Task<HttpResponseMessage> Get(int id)
        {
            HttpResponseMessage response;

            try
            {
                var cliente = _clienteRepository.Get(id);
                response = Request.CreateResponse(HttpStatusCode.OK, cliente);
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
                LogErrorHelper.Register(ex);
            }

            var tsc = new TaskCompletionSource<HttpResponseMessage>();
            tsc.SetResult(response);
            return tsc.Task;
        }
        
        [HttpGet]
        [Route("telefone/{telefone}")]
        public Task<HttpResponseMessage> GetByPhone(string telefone)
        {
            HttpResponseMessage response;

            try
            {
                var cliente = _clienteRepository.GetByPhone(telefone);
                response = Request.CreateResponse(HttpStatusCode.OK, cliente);
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
                LogErrorHelper.Register(ex);
            }

            var tsc = new TaskCompletionSource<HttpResponseMessage>();
            tsc.SetResult(response);
            return tsc.Task;
        }

        [HttpPost]
        [Route("")]
        public Task<HttpResponseMessage> Cadastrar(Cliente model)
        {
            var response = new HttpResponseMessage();

            try
            {
                var cliente = new Cliente(model.Nome, model.Telefone, model.Endereco, model.Bairro, model.Cep, model.Referencia);
                _clienteRepository.SaveOrUpdate(cliente);
                response = Request.CreateResponse(HttpStatusCode.OK, "Informações salvas com sucesso!");
            }
            catch (DbUpdateException ex)
            {
                if (ex.InnerException != null && (ex.InnerException.InnerException != null 
                                                  && (ex.InnerException != null 
                                                      && ex.InnerException.InnerException.Message.Contains("IX_CLIENTE_TELEFONE"))))
                    response = Request.CreateResponse(HttpStatusCode.BadRequest, "Este Telefone já está em uso por outro cliente.");
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
                LogErrorHelper.Register(ex);
            }

            var tsc = new TaskCompletionSource<HttpResponseMessage>();
            tsc.SetResult(response);
            return tsc.Task;
        }

        [HttpPut]
        [Authorize]
        [Route("")]
        public Task<HttpResponseMessage> Update(Cliente cliente)
        {
            var response = new HttpResponseMessage();

            try
            {
                _clienteRepository.SaveOrUpdate(cliente);

                response = Request.CreateResponse(HttpStatusCode.OK, "Informações salvas com sucesso!");
            }
            catch (DbUpdateException ex)
            {
                if (ex.InnerException != null && (ex.InnerException.InnerException != null 
                                                  && (ex.InnerException != null 
                                                      && ex.InnerException.InnerException.Message.Contains("IX_CLIENTE_TELEFONE"))))
                    response = Request.CreateResponse(HttpStatusCode.BadRequest, "Este Telefone já está em uso por outro cliente.");
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
                LogErrorHelper.Register(ex);
            }

            var tsc = new TaskCompletionSource<HttpResponseMessage>();
            tsc.SetResult(response);
            return tsc.Task;
        }

        [HttpPost]
        [Authorize]
        [Route("deletar/{id}")]
        public Task<HttpResponseMessage> Delete(int id)
        {
            var response = new HttpResponseMessage();

            try
            {
                _clienteRepository.Delete(id);

                response = Request.CreateResponse(HttpStatusCode.OK, "Informações removidas com sucesso!");
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
                LogErrorHelper.Register(ex);
            }

            var tsc = new TaskCompletionSource<HttpResponseMessage>();
            tsc.SetResult(response);
            return tsc.Task;
        }


        [HttpDelete]
        [Authorize]
        [Route("{id}")]
        public Task<HttpResponseMessage> Deletar(int id)
        {
            var response = new HttpResponseMessage();

            try
            {
                _clienteRepository.Delete(id);

                response = Request.CreateResponse(HttpStatusCode.OK, "Informações removidas com sucesso!");
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
                LogErrorHelper.Register(ex);
            }

            var tsc = new TaskCompletionSource<HttpResponseMessage>();
            tsc.SetResult(response);
            return tsc.Task;
        }


        protected override void Dispose(bool disposing)
        {
            _clienteRepository.Dispose();
        }
    }
}