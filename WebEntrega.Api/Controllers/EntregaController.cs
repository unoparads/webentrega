﻿using WebEntrega.Domain;
using WebEntrega.Domain.Repository;
using WebEntrega.Utils.Helpers;
using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace WebEntrega.Api.Controllers
{
    [RoutePrefix("api/entregas")]
    public class EntregaController : ApiController
    {
        private IEntregaRepository _entregaRepository;

        public EntregaController(IEntregaRepository entregaRepository)
        {
            _entregaRepository = entregaRepository;
        }

        [HttpGet]
        [Route("{dataInicial}/{dataFinal}")]
        public Task<HttpResponseMessage> GetByDate(DateTime dataInicial,DateTime DataFinal)
        {
            HttpResponseMessage response;

            try
            {
                var entregas = _entregaRepository.GetByDate(dataInicial, DataFinal);
                response = Request.CreateResponse(HttpStatusCode.OK, entregas);
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
                LogErrorHelper.Register(ex);
            }

            var tsc = new TaskCompletionSource<HttpResponseMessage>();
            tsc.SetResult(response);
            return tsc.Task;
        }
        [HttpGet]
        [Authorize]
        [Route("")]
        public Task<HttpResponseMessage> Get()
        {
            var response = new HttpResponseMessage();

            try
            {
                var entrega = _entregaRepository.GetAll();
                response = Request.CreateResponse(HttpStatusCode.OK, entrega);
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
                LogErrorHelper.Register(ex);
            }

            var tsc = new TaskCompletionSource<HttpResponseMessage>();
            tsc.SetResult(response);
            return tsc.Task;
        }

        [HttpGet]
        [Authorize]
        [Route("{id}")]
        public Task<HttpResponseMessage> Get(int id)
        {
            var response = new HttpResponseMessage();

            try
            {
                var entrega = _entregaRepository.GetById(id);
                response = Request.CreateResponse(HttpStatusCode.OK, entrega);
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
                LogErrorHelper.Register(ex);
            }

            var tsc = new TaskCompletionSource<HttpResponseMessage>();
            tsc.SetResult(response);
            return tsc.Task;
        }

        [HttpPost]
        [Authorize]
        [Route("")]
        public Task<HttpResponseMessage> Gravar(Entrega entrega)
        {
            var response = new HttpResponseMessage();

            try
            {
                _entregaRepository.SaveOrUpdate(entrega);
                response = Request.CreateResponse(HttpStatusCode.OK, "Sincronia realizada com sucesso!");
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
                LogErrorHelper.Register(ex);
            }

            var tsc = new TaskCompletionSource<HttpResponseMessage>();
            tsc.SetResult(response);
            return tsc.Task;
        }

        protected override void Dispose(bool disposing)
        {
            _entregaRepository.Dispose();
        }
    }
}
