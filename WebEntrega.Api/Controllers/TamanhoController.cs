﻿using System;
using System.Data.Entity.Infrastructure;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using WebEntrega.Domain;
using WebEntrega.Domain.Repository;
using WebEntrega.Utils.Helpers;

namespace WebEntrega.Api.Controllers
{
    [RoutePrefix("api/tamanhos")]
    public class TamanhoController : ApiController
    {
        private readonly ITamanhoRepository _tamanhoRepository;

        public TamanhoController(ITamanhoRepository tamanhoRepository)
        {
            _tamanhoRepository = tamanhoRepository;
        }

        [HttpGet]
        [Route("")]
        public Task<HttpResponseMessage> GetAll()
        {
            HttpResponseMessage response;

            try
            {
                var tamanhos = _tamanhoRepository.GetAll();
                response = Request.CreateResponse(HttpStatusCode.OK, tamanhos);
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
                LogErrorHelper.Register(ex);
            }

            var tsc = new TaskCompletionSource<HttpResponseMessage>();
            tsc.SetResult(response);
            return tsc.Task;
        }

        [HttpGet]
        [Route("{id}")]
        public Task<HttpResponseMessage> Get(int id)
        {
            HttpResponseMessage response;

            try
            {
                var tamanhos = _tamanhoRepository.Get(id);
                response = Request.CreateResponse(HttpStatusCode.OK, tamanhos);
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
                LogErrorHelper.Register(ex);
            }

            var tsc = new TaskCompletionSource<HttpResponseMessage>();
            tsc.SetResult(response);
            return tsc.Task;
        }
        
        
        [HttpPost]
        [Route("")]
        public Task<HttpResponseMessage> Cadastrar(Tamanho tamanho)
        {
            var response = new HttpResponseMessage();

            try
            {
                var tamanhos = new Tamanho(tamanho.Descricao);
                _tamanhoRepository.SaveOrUpdate(tamanhos);
                response = Request.CreateResponse(HttpStatusCode.OK, "Informações salvas com sucesso!");
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
                LogErrorHelper.Register(ex);
            }

            var tsc = new TaskCompletionSource<HttpResponseMessage>();
            tsc.SetResult(response);
            return tsc.Task;
        }

        [HttpPut]
        [Authorize]
        [Route("")]
        public Task<HttpResponseMessage> Update(Tamanho tamanho)
        {
            var response = new HttpResponseMessage();

            try
            {
                _tamanhoRepository.SaveOrUpdate(tamanho);

                response = Request.CreateResponse(HttpStatusCode.OK, "Informações salvas com sucesso!");
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
                LogErrorHelper.Register(ex);
            }

            var tsc = new TaskCompletionSource<HttpResponseMessage>();
            tsc.SetResult(response);
            return tsc.Task;
        }


        [HttpDelete]
        [Authorize]
        [Route("")]
        public Task<HttpResponseMessage> Delete(int tamanhoId)
        {
            var response = new HttpResponseMessage();

            try
            {
                _tamanhoRepository.Delete(tamanhoId);

                response = Request.CreateResponse(HttpStatusCode.OK, "Informações removidas com sucesso!");
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
                LogErrorHelper.Register(ex);
            }

            var tsc = new TaskCompletionSource<HttpResponseMessage>();
            tsc.SetResult(response);
            return tsc.Task;
        }


        protected override void Dispose(bool disposing)
        {
            _tamanhoRepository.Dispose();
        }
    }
}