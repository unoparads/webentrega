﻿using System;
using System.Data.Entity.Infrastructure;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using WebEntrega.Api.Models.Entregador;
using WebEntrega.Domain;
using WebEntrega.Domain.Repository;
using WebEntrega.Utils.Helpers;

namespace WebEntrega.Api.Controllers
{
    [RoutePrefix("api/Entregadores")]
    public class EntregadorController : ApiController
    {
        private readonly IEntregadorRepository _entregadorRepository;

        public EntregadorController(IEntregadorRepository entregadorRepository)
        {
            _entregadorRepository = entregadorRepository;
        }

        [HttpGet]
        [Route("")]
        public Task<HttpResponseMessage> GetAll()
        {
            HttpResponseMessage response;

            try
            {
                var entregador = _entregadorRepository.GetAll();
                response = Request.CreateResponse(HttpStatusCode.OK, entregador);
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
                LogErrorHelper.Register(ex);
            }

            var tsc = new TaskCompletionSource<HttpResponseMessage>();
            tsc.SetResult(response);
            return tsc.Task;
        }

        [HttpGet]
        [Route("")]
        public Task<HttpResponseMessage> Get(int id)
        {
            HttpResponseMessage response;

            try
            {
                var entregador = _entregadorRepository.Get(id);
                response = Request.CreateResponse(HttpStatusCode.OK, entregador);
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
                LogErrorHelper.Register(ex);
            }

            var tsc = new TaskCompletionSource<HttpResponseMessage>();
            tsc.SetResult(response);
            return tsc.Task;
        }
        
        [HttpGet]
        [Route("telefone/{telefone}")]
        public Task<HttpResponseMessage> GetByPhone(string telefone)
        {
            HttpResponseMessage response;

            try
            {
                var entregador = _entregadorRepository.GetByPhone(telefone);
                response = Request.CreateResponse(HttpStatusCode.OK, entregador);
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
                LogErrorHelper.Register(ex);
            }

            var tsc = new TaskCompletionSource<HttpResponseMessage>();
            tsc.SetResult(response);
            return tsc.Task;
        }

        [HttpPost]
        [Route("")]
        public Task<HttpResponseMessage> Cadastrar(AddEntregadorModel model)
        {
            var response = new HttpResponseMessage();

            try
            {
                var entregador = new Entregador(model.Nome, model.Telefone, model.Cnpj, model.Endereco, model.Bairro, model.Cep, model.email);
                _entregadorRepository.SaveOrUpdate(entregador);
                response = Request.CreateResponse(HttpStatusCode.OK, "Informações salvas com sucesso!");
            }
            catch (DbUpdateException ex)
            {
                if (ex.InnerException != null && (ex.InnerException.InnerException != null 
                                                  && (ex.InnerException != null 
                                                      && ex.InnerException.InnerException.Message.Contains("IX_ENTREGADOR_TELEFONE"))))
                    response = Request.CreateResponse(HttpStatusCode.BadRequest, "Este Telefone já está em uso por outro Entregador.");
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
                LogErrorHelper.Register(ex);
            }

            var tsc = new TaskCompletionSource<HttpResponseMessage>();
            tsc.SetResult(response);
            return tsc.Task;
        }

        [HttpPut]
        [Authorize]
        [Route("")]
        public Task<HttpResponseMessage> Update(Entregador entregador)
        {
            var response = new HttpResponseMessage();

            try
            {
                _entregadorRepository.SaveOrUpdate(entregador);

                response = Request.CreateResponse(HttpStatusCode.OK, "Informações salvas com sucesso!");
            }
            catch (DbUpdateException ex)
            {
                if (ex.InnerException != null && (ex.InnerException.InnerException != null 
                                                  && (ex.InnerException != null 
                                                      && ex.InnerException.InnerException.Message.Contains("IX_Entregador_TELEFONE"))))
                    response = Request.CreateResponse(HttpStatusCode.BadRequest, "Este Telefone já está em uso por outro Entregador.");
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
                LogErrorHelper.Register(ex);
            }

            var tsc = new TaskCompletionSource<HttpResponseMessage>();
            tsc.SetResult(response);
            return tsc.Task;
        }

        [HttpDelete]
        [Authorize]
        [Route("")]
        public Task<HttpResponseMessage> Delete(int entregadorId)
        {
            var response = new HttpResponseMessage();

            try
            {
                _entregadorRepository.Delete(entregadorId);

                response = Request.CreateResponse(HttpStatusCode.OK, "Informações removidas com sucesso!");
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
                LogErrorHelper.Register(ex);
            }

            var tsc = new TaskCompletionSource<HttpResponseMessage>();
            tsc.SetResult(response);
            return tsc.Task;
        }


        protected override void Dispose(bool disposing)
        {
            _entregadorRepository.Dispose();
        }
    }
}