﻿using WebEntrega.Data.Context;
using System.Web;

namespace WebEntrega.Api.Helpers
{
    public static class DataContextHelper
    {
        public static WebEntregaDataContext CurrentDataContext
        {
            get { return HttpContext.Current.Items["_EntityContext"] as WebEntregaDataContext; }
        }
    }
}