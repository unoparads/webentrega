﻿using System;

namespace WebEntrega.Utils.Helpers
{
    public static class LogErrorHelper
    {
        public static void Register(Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
}
