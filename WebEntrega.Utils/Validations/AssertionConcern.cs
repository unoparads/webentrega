﻿using System;
using System.Text.RegularExpressions;

namespace WebEntrega.Utils.Validations
{
    public class AssertionConcern
    {
        public static void AssertArgumentEquals(object object1, object object2, string message)
        {
            if (!object1.Equals(object2))
            {
                throw new InvalidOperationException(message);
            }
        }

        public static void AssertArgumentFalse(bool boolValue, string message)
        {
            if (boolValue)
            {
                throw new InvalidOperationException(message);
            }
        }

        public static void AssertArgumentLength(string stringValue, int maximum, string message)
        {
            var length = stringValue.Trim().Length;
            if (length > maximum)
            {
                throw new InvalidOperationException(message);
            }
        }

        public static void AssertArgumentLength(string stringValue, int minimum, int maximum, string message)
        {
            if (String.IsNullOrEmpty(stringValue))
                stringValue = String.Empty;

            var length = stringValue.Trim().Length;
            if (length < minimum || length > maximum)
            {
                throw new InvalidOperationException(message);
            }
        }

        public static void AssertArgumentMatches(string pattern, string stringValue, string message)
        {
            var regex = new Regex(pattern);

            if (!regex.IsMatch(stringValue))
            {
                throw new InvalidOperationException(message);
            }
        }

        public static void AssertArgumentNotEmpty(string stringValue, string message)
        {
            if (stringValue == null || stringValue.Trim().Length == 0)
            {
                throw new InvalidOperationException(message);
            }
        }

        public static void AssertArgumentNotEquals(object object1, object object2, string message)
        {
            if (object1.Equals(object2))
            {
                throw new InvalidOperationException(message);
            }
        }

        public static void AssertArgumentNotNull(object object1, string message)
        {
            if (object1 == null)
            {
                throw new InvalidOperationException(message);
            }
        }
        public static void AssertArgumentNotZeroOrNull(object object1, string message)
        {
            if (IsZeroOrNull(object1))
            {
                throw new InvalidOperationException(message);
            }
        }

        public static bool IsZeroOrNull(object pObject)
        {
            if (pObject is decimal)
            {
                return decimal.Round((decimal)pObject,6) == (decimal) 0.000000;
            }
            return ((pObject == null) ||(pObject == (Object)0) || (pObject == (Object)0.0) || (pObject.ToString() == "0.0") || (pObject.ToString() == "0"))  ;
        }

        public static void AssertArgumentNotEmptyOrNull(string stringValue, string message)
        {
            if (string.IsNullOrEmpty(stringValue))
            {
                throw new InvalidOperationException(message);
            }
        }

        public static void AssertArgumentMaximumRange(double value, double maximum, string message)
        {
            if (value > maximum)
            {
                throw new InvalidOperationException(message);
            }
        }

        public static void AssertArgumentMaximumRange(decimal value, decimal maximum, string message)
        {
            if (value > maximum)
            {
                throw new InvalidOperationException(message);
            }
        }

        public static void AssertArgumentMaximumRange(float value, float maximum, string message)
        {
            if (value > maximum)
            {
                throw new InvalidOperationException(message);
            }
        }

        public static void AssertArgumentMaximumRange(int value, int maximum, string message)
        {
            if (value > maximum)
            {
                throw new InvalidOperationException(message);
            }
        }

        public static void AssertArgumentMaximumRange(long value, long maximum, string message)
        {
            if (value > maximum)
            {
                throw new InvalidOperationException(message);
            }
        }

        public static void AssertArgumentMinimumRange(double value, double minimum, string message)
        {
            if (value < minimum)
            {
                throw new InvalidOperationException(message);
            }
        }

        public static void AssertArgumentMinimumRange(decimal value, decimal minimum, string message)
        {
            if (value < minimum)
            {
                throw new InvalidOperationException(message);
            }
        }
        public static void AssertArgumentMinimumRange(DateTime value, DateTime minimum, string message)
        {
            if (value < minimum)
            {
                throw new InvalidOperationException(message);
            }
        }
        public static void AssertArgumentMinimumRange(float value, float minimum, string message)
        {
            if (value < minimum)
            {
                throw new InvalidOperationException(message);
            }
        }

        public static void AssertArgumentMinimumRange(int value, int minimum, string message)
        {
            if (value < minimum)
            {
                throw new InvalidOperationException(message);
            }
        }

        public static void AssertArgumentMinimumRange(long value, long minimum, string message)
        {
            if (value < minimum)
            {
                throw new InvalidOperationException(message);
            }
        }

        public static void AssertArgumentRange(double value, double minimum, double maximum, string message)
        {
            if (value < minimum || value > maximum)
            {
                throw new InvalidOperationException(message);
            }
        }

        public static void AssertArgumentRange(decimal value, decimal minimum, decimal maximum, string message)
        {
            if (value < minimum || value > maximum)
            {
                throw new InvalidOperationException(message);
            }
        }

        public static void AssertArgumentRange(float value, float minimum, float maximum, string message)
        {
            if (value < minimum || value > maximum)
            {
                throw new InvalidOperationException(message);
            }
        }

        public static void AssertArgumentRange(int value, int minimum, int maximum, string message)
        {
            if (value < minimum || value > maximum)
            {
                throw new InvalidOperationException(message);
            }
        }

        public static void AssertArgumentRange(long value, long minimum, long maximum, string message)
        {
            if (value < minimum || value > maximum)
            {
                throw new InvalidOperationException(message);
            }
        }

        public static void AssertArgumentTrue(bool boolValue, string message)
        {
            if (!boolValue)
            {
                throw new InvalidOperationException(message);
            }
        }

        public static void AssertStateFalse(bool boolValue, string message)
        {
            if (boolValue)
            {
                throw new InvalidOperationException(message);
            }
        }

        public static void AssertStateTrue(bool boolValue, string message)
        {
            if (!boolValue)
            {
                throw new InvalidOperationException(message);
            }
        }

        protected AssertionConcern()
        {
        }

        protected void SelfAssertArgumentEquals(object object1, object object2, string message)
        {
            AssertArgumentEquals(object1, object2, message);
        }

        protected void SelfAssertArgumentFalse(bool boolValue, string message)
        {
            AssertArgumentFalse(boolValue, message);
        }

        protected void SelfAssertArgumentLength(string stringValue, int maximum, string message)
        {
            AssertArgumentLength(stringValue, maximum, message);
        }

        protected void SelfAssertArgumentLength(string stringValue, int minimum, int maximum, string message)
        {
            AssertArgumentLength(stringValue, minimum, maximum, message);
        }

        protected void SelfAssertArgumentMatches(string pattern, string stringValue, string message)
        {
            AssertArgumentMatches(pattern, stringValue, message);
        }

        protected void SelfAssertArgumentNotEmpty(string stringValue, string message)
        {
            AssertArgumentNotEmpty(stringValue, message);
        }

        protected void SelfAssertArgumentNotEquals(object object1, object object2, string message)
        {
            AssertArgumentNotEquals(object1, object2, message);
        }

        protected void SelfAssertArgumentNotNull(object object1, string message)
        {
            AssertArgumentNotNull(object1, message);
        }

        protected void SelfAssertArgumentRange(double value, double minimum, double maximum, string message)
        {
            AssertArgumentRange(value, minimum, maximum, message);
        }

        protected void SelfAssertArgumentRange(float value, float minimum, float maximum, string message)
        {
            AssertArgumentRange(value, minimum, maximum, message);
        }

        protected void SelfAssertArgumentRange(int value, int minimum, int maximum, string message)
        {
            AssertArgumentRange(value, minimum, maximum, message);
        }

        protected void SelfAssertArgumentRange(long value, long minimum, long maximum, string message)
        {
            AssertArgumentRange(value, minimum, maximum, message);
        }

        protected void SelfAssertArgumentTrue(bool boolValue, string message)
        {
            AssertArgumentTrue(boolValue, message);
        }

        protected void SelfAssertStateFalse(bool boolValue, string message)
        {
            AssertStateFalse(boolValue, message);
        }

        protected void SelfAssertStateTrue(bool boolValue, string message)
        {
            AssertStateTrue(boolValue, message);
        }
    }

}
