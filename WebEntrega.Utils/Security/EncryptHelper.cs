﻿using System.Security.Cryptography;
using System.Text;

namespace WebEntrega.Utils.Security
{
    public static class EncryptHelper
    {
        public static string CalculateMd5Hash(string input)
        {
            var md5 = MD5.Create();
            var inputBytes = Encoding.ASCII.GetBytes(input);
            var hash = md5.ComputeHash(inputBytes);

            var sb = new StringBuilder();
            for (var i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("X2"));
            }
            return sb.ToString();
        }

        public static string Encrypt(string input)
        {
            var salt = "b31da76c-8488-4f6f-966e-2369e9fbce03";
            var md5 = MD5.Create();
            var inputBytes = Encoding.ASCII.GetBytes(input + salt);
            var hash = md5.ComputeHash(inputBytes);

            var sb = new StringBuilder();
            for (var i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("X2"));
            }
            return sb.ToString();
        }
    }
}
