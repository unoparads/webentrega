﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;
using WebEntrega.Domain;

namespace WebEntrega.Data.Mapping
{
    internal class EntregadorMap : EntityTypeConfiguration<Entregador>
    {
        public EntregadorMap()
        {
            ToTable("Entregador");

            HasKey(x => x.Id);

            Property(x => x.Nome).IsRequired().HasMaxLength(160);
            Property(x => x.Telefone).IsRequired().HasMaxLength(20).HasColumnAnnotation("Index", new IndexAnnotation(new IndexAttribute("IX_ENTREGADOR_TELEFONE") {IsUnique = true}));
            Property(x => x.Cnpj).IsRequired().HasMaxLength(20).HasColumnAnnotation("Index", new IndexAnnotation(new IndexAttribute("IX_ENTREGADOR_CNPJ") {IsUnique = true}));
            Property(x => x.Endereco).IsRequired().HasMaxLength(200).IsFixedLength();
            Property(x => x.Cep).IsRequired().HasMaxLength(10).IsFixedLength();
            Property(x => x.Bairro).IsRequired().HasMaxLength(200).IsFixedLength();
            Property(x => x.Email).IsRequired().HasMaxLength(100).IsFixedLength();

            HasMany(x => x.Entregas);
        }
    }
}
