﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;
using WebEntrega.Domain;

namespace WebEntrega.Data.Mapping
{
    internal class ClienteMap : EntityTypeConfiguration<Cliente>
    {
        public ClienteMap()
        {
            ToTable("Cliente");

            HasKey(x => x.Id);

            Property(x => x.Nome).IsRequired().HasMaxLength(160);
            Property(x => x.Telefone).IsRequired().HasMaxLength(20).HasColumnAnnotation("Index", new IndexAnnotation(new IndexAttribute("IX_CLIENTE_TELEFONE") {IsUnique = true}));
            Property(x => x.Endereco).IsRequired().HasMaxLength(200).IsFixedLength();
            Property(x => x.Cep).IsRequired().HasMaxLength(10).IsFixedLength();
            Property(x => x.Bairro).IsRequired().HasMaxLength(200).IsFixedLength();
            Property(x => x.Referencia).IsRequired().HasMaxLength(1000).IsFixedLength();

            HasMany(x => x.Entregas);
        }
    }
}
