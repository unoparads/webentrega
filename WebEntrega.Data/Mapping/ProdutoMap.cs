﻿using System.Data.Entity.ModelConfiguration;
using WebEntrega.Domain;

namespace WebEntrega.Data.Mapping
{
    internal class ProdutoMap : EntityTypeConfiguration<Produto>
    {
        public ProdutoMap()
        {
            ToTable("Produto");

            HasKey(x => x.Id);

            Property(x => x.Nome).IsRequired().HasMaxLength(160);
            Property(x => x.Descricao).IsRequired().HasMaxLength(160);
            Property(x => x.TamanhoId).IsRequired();
            Property(x => x.Preco).IsRequired();
        }
    }
}
