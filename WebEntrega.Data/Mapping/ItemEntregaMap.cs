﻿using System.Data.Entity.ModelConfiguration;
using WebEntrega.Domain;

namespace WebEntrega.Data.Mapping
{
    internal class ItemEntregaMap : EntityTypeConfiguration<ItemEntrega>
    {
        public ItemEntregaMap()
        {
            ToTable("ItemEntrega");

            HasKey(x => x.Id);

            Property(x => x.Quantidade).IsRequired();
            Property(x => x.EntregaId).IsRequired();
            Property(x => x.ProdutoId).IsRequired();
            Property(x => x.Quantidade).IsRequired();
            Property(x => x.Valor).IsRequired();
        }
    }
}
