﻿using WebEntrega.Domain;
using System.Data.Entity.ModelConfiguration;

namespace WebEntrega.Data.Mapping
{
    public class EntregaMap : EntityTypeConfiguration<Entrega>
    {
        public EntregaMap()
        {
            ToTable("Entrega");

            HasKey(x => x.Id);

            Property(x => x.Entregue).IsRequired();
            Property(x => x.ClienteId).IsRequired();
            Property(x => x.Data).IsRequired();
            Property(x => x.EntregadorId).IsRequired();
            Property(x => x.Total).IsRequired();

            HasMany(x => x.ItensEntrega);
        }
    }
}
