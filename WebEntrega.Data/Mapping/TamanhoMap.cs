﻿using System.Data.Entity.ModelConfiguration;
using WebEntrega.Domain;

namespace WebEntrega.Data.Mapping
{
    internal class TamanhoMap : EntityTypeConfiguration<Tamanho>
    {
        public TamanhoMap()
        {
            ToTable("Tamanho");

            HasKey(x => x.Id);

            Property(x => x.Descricao).IsRequired().HasMaxLength(160);
            
        }
    }
}
