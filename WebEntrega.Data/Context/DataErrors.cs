﻿using System;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using WebEntrega.Utils.Helpers;

namespace WebEntrega.Data.Context
{
    public static class DataErrors
    {
        public static void CapturaErros(DbEntityValidationException ex)
        {
            var errors = new StringBuilder();
            foreach (var validationError
                in ex.EntityValidationErrors
                    .SelectMany(entityValidationErrors => entityValidationErrors.ValidationErrors))
                errors.Append("Property: " + validationError.PropertyName + " Error: " + validationError.ErrorMessage);
            Console.WriteLine(errors);
            LogErrorHelper.Register(ex);
        }
    }
}
