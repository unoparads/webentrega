﻿using WebEntrega.Data.Mapping;
using WebEntrega.Domain;
using System.Data.Entity;

namespace WebEntrega.Data.Context
{
    public class WebEntregaDataContext : DbContext
    {
        public WebEntregaDataContext()
            : base("WebEntregaConnectionString")
        {
            Configuration.LazyLoadingEnabled = false;
            Configuration.ProxyCreationEnabled = false;
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Cliente> Clientes { get; set; }
        public DbSet<Entregador> Entregadores { get; set; }
        public DbSet<Entrega> Entregas { get; set; }
        public DbSet<ItemEntrega> ItensEntrega { get; set; }
        public DbSet<Produto> Produtos { get; set; }
        public DbSet<Tamanho> Tamanhos { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new UserMap());
            modelBuilder.Configurations.Add(new EntregaMap());
            modelBuilder.Configurations.Add(new ClienteMap());
            modelBuilder.Configurations.Add(new EntregadorMap());
            modelBuilder.Configurations.Add(new ItemEntregaMap());
            modelBuilder.Configurations.Add(new ProdutoMap());
            modelBuilder.Configurations.Add(new TamanhoMap());

        }
    }
}
