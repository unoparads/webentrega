﻿using System.Data.Entity;

namespace WebEntrega.Data.Context
{
    public class WebEntregaDataContextInitializer : DropCreateDatabaseAlways<WebEntregaDataContext>
    {
        protected override void Seed(WebEntregaDataContext context)
        {

        }
    }
}