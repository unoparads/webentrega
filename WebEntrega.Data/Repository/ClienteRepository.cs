﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using WebEntrega.Data.Context;
using WebEntrega.Domain;
using WebEntrega.Domain.Repository;
using WebEntrega.Utils.Helpers;

namespace WebEntrega.Data.Repository
{
    public class ClienteRepository : IClienteRepository
    {
        private readonly WebEntregaDataContext _context;

        public ClienteRepository(WebEntregaDataContext context)
        {
            _context = context;
        }

        public Cliente Get(int id)
        {
            return _context.Clientes.FirstOrDefault(x => x.Id == id);
        }

        public IList<Cliente> GetByName(string nome)
        {
            return new List<Cliente>(_context.Clientes.Where(x => x.Nome.Contains(nome)));
        }

        public IList<Cliente> GetByPhone(string telefone)
        {
            return new List<Cliente>(_context.Clientes.Where(x => x.Telefone.Contains(telefone)));
        }

        public void SaveOrUpdate(Cliente cliente)
        {
            if (cliente.Id == 0)
                _context.Clientes.Add(cliente);
            else
                _context.Entry(cliente).State = EntityState.Modified;

            try
            {
                _context.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                DataErrors.CapturaErros(ex);
            }
            catch (Exception ex)
            {
                LogErrorHelper.Register(ex);
            }
        }

        public void Delete(int id)
        {
            _context.Clientes.Remove(_context.Clientes.Find(id));
            _context.SaveChanges();
        }

        public IList<Cliente> GetAll()
        {
            return new List<Cliente>(_context.Clientes);
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
