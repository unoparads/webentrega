﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using WebEntrega.Data.Context;
using WebEntrega.Domain;
using WebEntrega.Domain.Repository;
using WebEntrega.Utils.Helpers;

namespace WebEntrega.Data.Repository
{
    public class ProdutoRepository : IProdutoRepository
    {
        private readonly WebEntregaDataContext _context;

        public ProdutoRepository(WebEntregaDataContext context)
        {
            _context = context;
        }

        public Produto Get(int id)
        {
            return _context.Produtos.FirstOrDefault(x => x.Id == id);
        }

        public IList<Produto> GetByName(string nome)
        {
            return new List<Produto>(_context.Produtos.Where(x => x.Nome.Contains(nome)));
        }

        public IList<Produto> GetByDescricao(string descricao)
        {
            return new List<Produto>(_context.Produtos.Where(x => x.Descricao.Contains(descricao)));
        }

        public IList<Produto> GetByTamanho(int tamanhoId)
        {
            return new List<Produto>(_context.Produtos.Where(x => x.TamanhoId == tamanhoId));
        }

        public void SaveOrUpdate(Produto produto)
        {
            if (produto.Id == 0)
                _context.Produtos.Add(produto);
            else
                _context.Entry(produto).State = EntityState.Modified;

            try
            {
                _context.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                DataErrors.CapturaErros(ex);
            }
            catch (Exception ex)
            {
                LogErrorHelper.Register(ex);
            }
        }

        public void Delete(int id)
        {
            _context.Produtos.Remove(_context.Produtos.Find(id));
            _context.SaveChanges();
        }

        public IList<Produto> GetAll()
        {
            return new List<Produto>(_context.Produtos);
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
