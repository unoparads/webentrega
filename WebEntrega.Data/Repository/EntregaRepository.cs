﻿using System;
using WebEntrega.Data.Context;
using WebEntrega.Domain;
using WebEntrega.Domain.Repository;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using WebEntrega.Utils.Helpers;

namespace WebEntrega.Data.Repository
{
    public class EntregaRepository : IEntregaRepository
    {
        private readonly WebEntregaDataContext _context;

        public EntregaRepository(WebEntregaDataContext context)
        {
            _context = context;
        }

        public IList<Entrega> GetById(int id)
        {
            var entregas = _context
                .Entregas
                .Include(x => x.ItensEntrega)
                .Where(x => x.Id == id);

            return entregas.ToList();
        }

        public IList<Entrega> GetAll()
        {
            var entregas = _context
                .Entregas
                .Include(x => x.ItensEntrega);

            return entregas.ToList();
        }

        public IList<Entrega> GetByEntregadorId(int entregadorId)
        {
            var entregador = _context
                .Entregadores
                .Include(x => x.Entregas)
                .FirstOrDefault(x => x.Id == entregadorId);

            if (entregador != null)
                return entregador.Entregas.ToList();
            else
                return new List<Entrega>();
        }

        public IList<Entrega> GetByDate(DateTime dataInicial, DateTime dataFinal)
        {
            var entregas = _context
                .Entregas
                .Include(x => x.ItensEntrega)
                .Where(x => x.Data >= dataInicial && x.Data <= dataFinal);

            return entregas.ToList();
        }

        public void SaveOrUpdate(Entrega entrega)
        {
            if (entrega.Id == 0)
                _context.Entregas.Add(entrega);
            else
                _context.Entry(entrega).State = EntityState.Modified;

            try
            {
                _context.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                DataErrors.CapturaErros(ex);
            }
            catch (Exception ex)
            {
                LogErrorHelper.Register(ex);
            }
        }
        
        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
