﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using WebEntrega.Data.Context;
using WebEntrega.Domain;
using WebEntrega.Domain.Repository;
using WebEntrega.Utils.Helpers;

namespace WebEntrega.Data.Repository
{
    public class TamanhoRepository : ITamanhoRepository
    {
        private readonly WebEntregaDataContext _context;

        public TamanhoRepository(WebEntregaDataContext context)
        {
            _context = context;
        }

        public Tamanho Get(int id)
        {
            return _context.Tamanhos.FirstOrDefault(x => x.Id == id);
        }

        public void SaveOrUpdate(Tamanho tamanho)
        {
            if (tamanho.Id == 0)
                _context.Tamanhos.Add(tamanho);
            else
                _context.Entry(tamanho).State = EntityState.Modified;

            try
            {
                _context.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                DataErrors.CapturaErros(ex);
            }
            catch (Exception ex)
            {
                LogErrorHelper.Register(ex);
            }
        }

        public void Delete(int id)
        {
            _context.Tamanhos.Remove(_context.Tamanhos.Find(id));
            _context.SaveChanges();
        }

        public IList<Tamanho> GetAll()
        {
            return new List<Tamanho>(_context.Tamanhos);
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
