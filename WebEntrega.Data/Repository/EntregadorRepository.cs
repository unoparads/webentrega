﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using WebEntrega.Data.Context;
using WebEntrega.Domain;
using WebEntrega.Domain.Repository;
using WebEntrega.Utils.Helpers;

namespace WebEntrega.Data.Repository
{
    public class EntregadorRepository : IEntregadorRepository
    {
        private readonly WebEntregaDataContext _context;

        public EntregadorRepository(WebEntregaDataContext context)
        {
            _context = context;
        }

        public Entregador Get(int id)
        {
            return _context.Entregadores.FirstOrDefault(x => x.Id == id);
        }

        public IList<Entregador> GetByName(string nome)
        {
            return new List<Entregador>(_context.Entregadores.Where(x => x.Nome.Contains(nome)));
        }

        public IList<Entregador> GetByPhone(string telefone)
        {
            return new List<Entregador>(_context.Entregadores.Where(x => x.Telefone.Contains(telefone)));
        }

        public void SaveOrUpdate(Entregador entregador)
        {
            if (entregador.Id == 0)
                _context.Entregadores.Add(entregador);
            else
                _context.Entry(entregador).State = EntityState.Modified;

            try
            {
                _context.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                DataErrors.CapturaErros(ex);
            }
            catch (Exception ex)
            {
                LogErrorHelper.Register(ex);
            }
        }

        public void Delete(int id)
        {
            _context.Entregadores.Remove(_context.Entregadores.Find(id));
            _context.SaveChanges();
        }

        public IList<Entregador> GetAll()
        {
            return new List<Entregador>(_context.Entregadores);
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
