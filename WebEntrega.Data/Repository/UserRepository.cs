﻿using System;
using WebEntrega.Data.Context;
using WebEntrega.Domain;
using WebEntrega.Domain.Repository;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using WebEntrega.Utils.Helpers;

namespace WebEntrega.Data.Repository
{
    public class UserRepository : IUserRepository
    {
        private readonly WebEntregaDataContext _context;

        public UserRepository(WebEntregaDataContext context)
        {
            _context = context;
        }

        public User Get(string email)
        {
            return _context.Users.Where(x => x.Email.ToLower() == email.ToLower()).FirstOrDefault();
        }

        public void SaveOrUpdate(User user)
        {
            try
            {
                if (user.Id == 0)
                    _context.Users.Add(user);
                else
                    _context.Entry(user).State = EntityState.Modified;

                _context.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                DataErrors.CapturaErros(ex);
            }
            catch (Exception ex)
            {
                LogErrorHelper.Register(ex);
            }
        }

        public void Delete(int id)
        {
            _context.Users.Remove(_context.Users.Find(id));
            _context.SaveChanges();
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
