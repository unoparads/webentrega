namespace WebEntrega.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EstruturaInicial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Cliente",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nome = c.String(nullable: false, maxLength: 160),
                        Telefone = c.String(nullable: false, maxLength: 20),
                        Endereco = c.String(nullable: false, maxLength: 200, fixedLength: true),
                        Bairro = c.String(nullable: false, maxLength: 200, fixedLength: true),
                        Cep = c.String(nullable: false, maxLength: 10, fixedLength: true),
                        Referencia = c.String(nullable: false, maxLength: 1000, fixedLength: true),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Telefone, unique: true, name: "IX_CLIENTE_TELEFONE");
            
            CreateTable(
                "dbo.Entrega",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Entregue = c.Boolean(nullable: false),
                        ClienteId = c.Int(nullable: false),
                        EntregadorId = c.Int(nullable: false),
                        Total = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Data = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Cliente", t => t.ClienteId, cascadeDelete: true)
                .ForeignKey("dbo.Entregador", t => t.EntregadorId, cascadeDelete: true)
                .Index(t => t.ClienteId)
                .Index(t => t.EntregadorId);
            
            CreateTable(
                "dbo.ItemEntrega",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        EntregaId = c.Int(nullable: false),
                        ProdutoId = c.Int(nullable: false),
                        Quantidade = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Valor = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Entrega", t => t.EntregaId, cascadeDelete: true)
                .Index(t => t.EntregaId);
            
            CreateTable(
                "dbo.Entregador",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nome = c.String(nullable: false, maxLength: 160),
                        Cnpj = c.String(nullable: false, maxLength: 20),
                        Telefone = c.String(nullable: false, maxLength: 20),
                        Endereco = c.String(nullable: false, maxLength: 200, fixedLength: true),
                        Bairro = c.String(nullable: false, maxLength: 200, fixedLength: true),
                        Cep = c.String(nullable: false, maxLength: 10, fixedLength: true),
                        Email = c.String(nullable: false, maxLength: 100, fixedLength: true),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Cnpj, unique: true, name: "IX_ENTREGADOR_CNPJ")
                .Index(t => t.Telefone, unique: true, name: "IX_ENTREGADOR_TELEFONE");
            
            CreateTable(
                "dbo.Produto",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nome = c.String(nullable: false, maxLength: 160),
                        Descricao = c.String(nullable: false, maxLength: 160),
                        TamanhoId = c.Int(nullable: false),
                        Preco = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Tamanho",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Descricao = c.String(nullable: false, maxLength: 160),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.User",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 60),
                        Email = c.String(nullable: false, maxLength: 160),
                        Password = c.String(nullable: false, maxLength: 32, fixedLength: true),
                        IsActive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Email, unique: true, name: "IX_USER_EMAIL");
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Entrega", "EntregadorId", "dbo.Entregador");
            DropForeignKey("dbo.Entrega", "ClienteId", "dbo.Cliente");
            DropForeignKey("dbo.ItemEntrega", "EntregaId", "dbo.Entrega");
            DropIndex("dbo.User", "IX_USER_EMAIL");
            DropIndex("dbo.Entregador", "IX_ENTREGADOR_TELEFONE");
            DropIndex("dbo.Entregador", "IX_ENTREGADOR_CNPJ");
            DropIndex("dbo.ItemEntrega", new[] { "EntregaId" });
            DropIndex("dbo.Entrega", new[] { "EntregadorId" });
            DropIndex("dbo.Entrega", new[] { "ClienteId" });
            DropIndex("dbo.Cliente", "IX_CLIENTE_TELEFONE");
            DropTable("dbo.User");
            DropTable("dbo.Tamanho");
            DropTable("dbo.Produto");
            DropTable("dbo.Entregador");
            DropTable("dbo.ItemEntrega");
            DropTable("dbo.Entrega");
            DropTable("dbo.Cliente");
        }
    }
}
